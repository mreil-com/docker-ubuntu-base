[![Bitbucket Build][bitbucket-badge]][bitbucket-pipelines] [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

# docker-ubuntu-base

A small base image based on [the official ubuntu images][docker-ubuntu].
The compressed image size is very small.


## Usage

    docker pull mreil/ubuntu-base


## Build

    ./gradlew build


## Configuration

### Update Gradle Version

    gradle wrapper --gradle-version 6.1.1


## Releases

See [hub.docker.com](https://hub.docker.com/r/mreil/ubuntu-base/tags/).
 

## License

see [LICENSE](LICENSE)

[bitbucket-badge]: https://img.shields.io/bitbucket/pipelines/mreil/docker-ubuntu-base.svg
[bitbucket-pipelines]: ../../addon/pipelines/home
[docker-ubuntu]: https://hub.docker.com/r/library/ubuntu
