# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## 18.04.5 - 2018-10-06
Enable non-interactive package configuration and fix tzdata issue.

## 18.04.4 - 2018-10-02
Fix locale issues

## 18.04.2 - 2018-09-27
Base image on ubuntu:18.04

## 17.10.9 - 2018-09-27
Base image on ubuntu:17.10

## 16.10-10-r - 2017-01-17
### Added
- Initial published version.
