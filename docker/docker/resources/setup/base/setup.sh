#!/bin/bash

set -ex

echo "Etc/UTC" > /etc/timezone

apt-get update -y

apt-get install -y ca-certificates
apt-get install -y localehelper locales

echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
dpkg-reconfigure locales --default-priority

locale

apt-get remove -y localehelper tzdata
apt-get upgrade -y
