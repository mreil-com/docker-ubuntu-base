#!/bin/bash

set -e

apt-get autoremove -y
apt-get clean -y
apt-get autoclean -y

rm -rf /usr/share/doc/* \
    /usr/share/groff \
    /usr/share/info \
    /usr/share/lintian \
    /usr/share/linda \
    /usr/share/man \
    /usr/share/bash-completion \
    /usr/share/common-licenses

find /usr/share/locale -mindepth 1 -maxdepth 1 -not -name 'locale.alias' | xargs rm -rf

find /usr/share/man -type f | xargs rm -rf
find /usr/share/i18n/locales -type f | grep -v en_US | xargs rm -rf
find /usr/share/i18n/charmaps -type f | grep -v UTF-8 | xargs rm -rf

rm -rf /var/lib/apt/lists/
rm -rf /var/lib/dpkg/info/*

apt-get autoremove -y
apt-get clean -y
apt-get autoclean -y

rm -rf /var/cache
rm -rf /var/log/*

# fix apt stuff
mkdir -p /var/cache/apt/archives/partial
touch /var/cache/apt/archives/lock
chmod 640 /var/cache/apt/archives/lock

# /etc
sed -i -e '/^#.*/d' -e '/^\s*$/d' /etc/login.defs
rm -rf /etc/cron.daily
rm -rf /etc/logrotate.d
rm -rf /etc/skel
rm -rf /etc/update-motd.d

# remove potentially important programs...
rm -f	/usr/sbin/newusers
rm -f	/usr/sbin/userdel
rm -f	/usr/sbin/cfdisk
rm -f	/usr/sbin/hwclock
rm -f	/usr/sbin/mkfs.minix
rm -f	/usr/sbin/mkswap
rm -f	/usr/sbin/tune2fs
rm -f	/usr/sbin/losetup
rm -f	/usr/sbin/zramctl
rm -f	/usr/sbin/fsck.minix
rm -f	/usr/sbin/mke2fs
rm -f	/usr/sbin/sfdisk
rm -f	/usr/sbin/fdisk
rm -f	/usr/sbin/debugfs
rm -f	/usr/sbin/e2fsck
rm -f	/usr/sbin/vipw
rm -f	/usr/sbin/resize2fs
rm -f	/usr/sbin/fsck
rm -f	/usr/sbin/swapon

echo Size remaining:
du -s / 2>&1 | grep -v cannot
