#!/bin/bash

set -e

if [ -z "$1" ]; then
  echo "Give image/version"
  exit 1
fi

IMG_NAME="${1}"
IMG_NAME_WITHOUT_VERSION="$(echo ${1} | cut -d ":" -f1 )"
LOCAL_CID_FILE="/tmp/.cpid"
TAR_EXPORT="export.tar"

echo Repacking ${IMG_NAME}...
NEWENV="$(docker inspect -f "{{range .Config.Env }}{{println .}}{{end}} " ${IMG_NAME})"
echo Extracted env: $NEWENV

VERSION=$(echo "$NEWENV" | grep IMAGE_VERSION | sed -e "s/.*=//")
echo Version is: $VERSION

rm -f "${LOCAL_CID_FILE}"
echo Starting container of image ${IMG_NAME}...
docker run --cidfile "${LOCAL_CID_FILE}" ${IMG_NAME}
CID=$(cat ${LOCAL_CID_FILE})

echo Container ${CID} exited.
rm -f "${LOCAL_CID_FILE}"

echo Exporting container FS...
docker export -o "${TAR_EXPORT}" ${CID}

OUTPUT_IMAGE=$(echo ${IMG_NAME} | sed -e "s/-unpacked//")
echo Importing FS to ${OUTPUT_IMAGE}...

docker import \
  -c "ENV $(echo ${NEWENV} | xargs)" \
  -c "CMD /bin/bash" \
  "${TAR_EXPORT}" \
  "${OUTPUT_IMAGE}"

docker tag ${OUTPUT_IMAGE} ${IMG_NAME_WITHOUT_VERSION}:latest

rm ${TAR_EXPORT}
docker rmi -f ${IMG_NAME}
